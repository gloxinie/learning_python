myName = input('Enter your name: ')
print('myName:', myName)
print('type of myName:', type(myName))
print('Your name has', len(myName), 'characters.')


def choose_cave():
    cave = ''
    while cave != '1' and cave != '2':
        print('Which cave will you got into? (1 or 2)?')
        cave = input()
    return cave


choose_cave()


spam = 0
while spam < 5:
    spam = spam + 1
    if spam == 3:
        continue    # jumps immediately back to the while loop without executing anything below
    print('spam is:', spam)


for i in range(3, 37, 3): # start 3, stop before 37, step 3
    print(i)

print('How many cats do you have?')
num_cats = input()
try:
    if int(num_cats) >= 4:
        print('That is a lot of cats!')
    elif int(num_cats) >= 0:
        print('That is not many cats.')
    else:
        print('Please enter a positive number or zero.')
except ValueError:
    print('Please enter a number.')
