from datetime import datetime
import glob
import os
import shutil
import tkinter
from tkinter import simpledialog
from tkinter import messagebox


# VARIABLES
src_dir = '/media/andy/Daten/test/test1'
target_dir = '/media/andy/Daten/test/test4'
no_dir = 'dir cannot be created'

# LIST FILES
file_names = os.listdir(src_dir)  # list files in this dir (not recursive!)
# print('files in dir test1:', file_names)  # 'bemoved.md', 'libraries & code.md', 'recursive', '__main__.py']

# LIST PATH, DIRS & FILES RECURSIVELY
file_names_rec = list(os.walk(src_dir))
# print('files recursively in dir1:', file_names_rec)
# [('/media/andy/Daten/test/test1', ['recursive'], ['bemoved.md', 'libraries & code.md',
# '__main__.py']), ('/media/andy/Daten/test/test1/recursive', [], ['Stunden.md', 'Übersicht.pdf'])]

# LIST PATHS RECURSIVELY
for filename in glob.glob(src_dir + '*/**', recursive=True):
    # print('glob:', filename)
    pass
# glob: /media/andy/Daten/test/test1/
# glob: /media/andy/Daten/test/test1/bemoved.md
# glob: /media/andy/Daten/test/test1/libraries & code.md
# glob: /media/andy/Daten/test/test1/recursive
# glob: /media/andy/Daten/test/test1/recursive/Stunden.md
# glob: /media/andy/Daten/test/test1/recursive/Übersicht.pdf
# glob: /media/andy/Daten/test/test1/__main__.py

# LIST PATHS ONLY PDFs RECURSIVELY
for filename in glob.glob(src_dir + '/**/*.pdf', recursive=True):
    # print('glob only pdf:', filename)  # /media/andy/Daten/test/test1/recursive/Übersicht.pdf
    pass

# LIST PATH, DIRS & FILES RECURSIVELY STRUCTURED:
for path, dirs, files in os.walk(src_dir):
    # print('path in test1:', path)  # /media/andy/Daten/test/test1, media / andy / Daten / test / test1 / recursive
    # print('dirs in test1:', dirs)  # ['recursive'], []
    # print('files in test1:', files)  # ['bemoved.md', 'libraries & code.md', '__main__.py'], ['Stunden.md',
    # 'Übersicht.pdf']
    pass

# os.mkdir('/media/andy/Daten/test/test3') # create dir 'test3'
# os.mkdir('/media/andy/Daten/test/test3/subdir') # create subdir 'subdir'
# os.makedirs('/media/andy/Daten/test/test3/subdir') # create dir ('test3') and subdir ('subdir') in one go


# CREATE DIRS AND SUBDIRS
# create 'pat_ordner' with 4 subdirs
subfolder_names = ['Grail', 'Motion Metrix', 'Videos', 'Zebris']
for subfolder_name in subfolder_names:
    try:
        os.makedirs(os.path.join('/media/andy/Daten/test/pat_ordner', subfolder_name), exist_ok=True)
    except OSError as error:
        print(no_dir)

# create 2 subdirs in 'Videos'-subdir
subfolder_videos = ['Bilder', 'Videos']
for subfolder_video in subfolder_videos:
    try:
        os.makedirs(os.path.join('/media/andy/Daten/test/pat_ordner/Videos', subfolder_video), exist_ok=True)
    except OSError as error:
        print(no_dir)

# create 3 subdirs in 'Zebris'-subdir
subfolder_zebris = ['jwi_report', 'pdfs', 'xml']
for subfolder_zebri in subfolder_zebris:
    try:
        os.makedirs(os.path.join('/media/andy/Daten/test/pat_ordner/Zebris', subfolder_zebri), exist_ok=True)
    except OSError as error:
        print(no_dir)

# COPY FILES AND DIRS

# Copy complete dir with subdirs
try:
    # copy everything from src_dir to target_dir, even if target_dir already exists
    shutil.copytree(src_dir, target_dir, symlinks=True, dirs_exist_ok=True)
except error as e:  # might not be the proper syntax
    print('copytree did not work:', e)

# copy all pdf-files
for filename in glob.glob(src_dir + '/**/*.pdf', recursive=True):
    print('filename pdf:', filename)
    try:
        os.makedirs(os.path.join('/media/andy/Daten/test/test5'), exist_ok=True)
        shutil.copy2(filename, '/media/andy/Daten/test/test5')
    except OSError as error:
        print(no_dir)

# ASK USER FOR ID OF PATIENT, CREATE DIRS AND COPY THEM TO ANOTHER PATH

# create and hide main tkinter window
root = tkinter.Tk()
root.withdraw()

# get id of patient
while True:
    try:
        pat_id = int(tkinter.simpledialog.askstring(title='Patient:innennummer',
                                                    prompt='Wie ist die Nummer der:s Patient:in?'))
    except ValueError:
        messagebox.showerror('Fehler', 'Es muss eine ganze Zahl eingegeben werden.')
        print('Es muss eine ganze Zahl eingegeben werden.')
        continue
    break

# get date of today
date_today = datetime.today().strftime('%Y%m%d')

# create dir of patient and date
os.makedirs(f'/media/andy/Daten/test/DLST_P{pat_id}/{date_today}')
patient_dir = f'/media/andy/Daten/test/DLST_P{pat_id}/{date_today}'

# create 'pat_ordner' with 4 subdirs
subfolder_names = ['Grail', 'Motion Metrix', 'Videos', 'Zebris']
for subfolder_name in subfolder_names:
    try:
        os.makedirs(os.path.join(f'{patient_dir}', subfolder_name), exist_ok=True)
    except OSError as error:
        print(no_dir)

# create 2 subdirs in 'Videos'-subdir
subfolder_videos = ['Bilder', 'Videos']
for subfolder_video in subfolder_videos:
    try:
        os.makedirs(os.path.join(f'{patient_dir}/Videos', subfolder_video), exist_ok=True)
    except OSError as error:
        print(no_dir)

# create 3 subdirs in 'Zebris'-subdir
subfolder_zebris = ['jwi_report', 'pdfs', 'xml']
for subfolder_zebri in subfolder_zebris:
    try:
        os.makedirs(os.path.join(f'{patient_dir}/Zebris', subfolder_zebri), exist_ok=True)
    except OSError as error:
        print(no_dir)

# Copy complete dir with subdirs
src_dir = f'/media/andy/Daten/test/DLST_P{pat_id}'
target_dir = f'/media/andy/Daten/test/test2/DLST_P{pat_id}'

try:
    shutil.copytree(src_dir, target_dir, symlinks=True, dirs_exist_ok=True)  # copies everything in src_dir to
    # everything in src_dir to
    # target_dir, even if target_dir already exists
    # check whether symlinks should be True or False (?)
except error as e:  # might not be the proper syntax
    print('copytree did not work:', e)
