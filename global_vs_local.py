def outer():
    x = "local"
    print("x in outer before inner:", x)

    def inner():
        nonlocal x
        x = "nonlocal"
        print("x in inner:", x)

    inner()
    print("x in outer after inner:", x)

outer()

print("x outside outer:", x)
