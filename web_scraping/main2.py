import requests
from bs4 import BeautifulSoup

url = 'http://books.toscrape.com/index.html'
response = requests.get(url)
html = response.content
scraped = BeautifulSoup(html, 'html.parser')

title = scraped.title.text.strip()
# print(title)

first_link = scraped.a
# print(first_link)

attribute_of_link = scraped.a['href']

article = scraped.article
# print(article)
title_of_first_article = article.h3.a['title']
# print(title_of_first_article)

# filter for all 'article' tags that have the attribute 'product_pod'
# find_all_articles = scraped.find_all("article", class_="product_pod")
# print(find_all_articles)
# same way to do it:
find_all_articles = scraped.select('.product_pod')
# print(find_all_articles)

for find_article in find_all_articles:
    article_titles = find_article.h3.a['title']
    # print(article_titles)

for article in find_all_articles:
    # use in-built function 'find'
    price = article.find('p', class_='price_color').text
    # print(price)

# get html-structure of site:
# for child in scraped.children:
    # print(child)

# connect book title and price
for article in find_all_articles:
    title = article.h3.a['title']
    price = article.find('p', class_='price_color').text
    # print(title, price)

ages = [25, 28, 17, 32]
# print(ages[-1])   # 32

student = {'name': 'Bob', 'age': 24}
students = [
    {'name': 'Bob', 'age': 24},
    {'name': 'Alice', 'age': 32},
    {'name': 'Charlie', 'age': 17}
]
for student in students:
    name_age = (student['name'], student['age'])
    # print(name_age)

# get a dictionary with name of book and corresponding price
all_books = []
for article in find_all_articles:
    title = article.h3.a['title']
    price = article.find('p', class_='price_color').text
    all_books.append({title: price})
# print(all_books)