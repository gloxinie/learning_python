from login_form import Ui_Form
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc


class LoginWindow(qtw.QWidget, Ui_Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setupUi(self)

        self.login_button.clicked.connect(self.authenticate)


    def authenticate(self):

        username = self.username_edit.text() 
        password = self.password_edit.text()

        if username == 'user' and password == 'pass':
            qtw.QMessageBox.information(self, 'Success', 'You are logged in')
            # QMessageBox ist pop-up-window
            # information: blaues Ausrufezeichen
            # 'Success' ist der Titel der Box
            # 'You are logged in' ist der Text im Hauptfeld der Box
        else:
            qtw.QMessageBox.critical(self, 'Fail', 'Login failed')
            # critical: rotes Sperrzeichen


if __name__ == "__main__":
    app = qtw.QApplication([])

    widget = LoginWindow()
    widget.show()

    app.exec_()


    # import sys
    # app = QtWidgets.QApplication(sys.argv)
    # Form = QtWidgets.QWidget()
    # ui = Ui_Form()
    # ui.setupUi(Form)
    # Form.show()
    # sys.exit(app.exec_())



