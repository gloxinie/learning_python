import PyQt5.QtWidgets as qtw
import PySide2.QtCore
import sys
import random
import time
import platform
from PySide2 import QtCore, QtWidgets, QtGui
# QtScript is deprecated since Qt 5.5

print("PySide2 version:                   ", PySide2.__version__)
print("tuple with each version component: ",  PySide2.__version_info__)
print("Qt version used to compile PySide2:", PySide2.QtCore.__version__)
print("tuple with each version components of Qt used to compile PySide2:", PySide2.QtCore.__version_info__)
print("Time:                              ",time.ctime())
print("Python version:                     {}".format(sys.version.splitlines()[0]))
print("Platform used:                      {}".format(platform.platform()))

# Close pop-up when no macOS
# try:
#     from PySide2 import QtMacExtras
# except ImportError:
#     app = QtWidgets.QApplication(sys.argv)
#     messageBox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "QtMacExtras macpasteboardmime",
#                                        "This example only runs on macOS and QtMacExtras must be installed to run this example.",
#                                        QtWidgets.QMessageBox.Close)
#     messageBox.exec_()
#     sys.exit(1)

class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.hello = ["Hallo Welt", "Hei maailma", "Hola Mundo", "Привет мир"]

        self.button = QtWidgets.QPushButton("Click me!")
        self.text = QtWidgets.QLabel("Hello World")
        self.text.setAlignment(QtCore.Qt.AlignCenter)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

        self.button.clicked.connect(self.magic)


    def magic(self):
        self.text.setText(random.choice(self.hello))


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MyWidget()
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec_())