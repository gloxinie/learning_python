# Python - good to know

### Naming modules and functions

100% verstanden habe ich das mit diesem Statement auch nicht.
Grundlegend schreibe ich jedoch die folgenden Zeilen an das Ende jedes Moduls, was ich "von außen" (z.B. über Command-line) ausführen möchte.
Das kann das "Eintrittsmodul" eines Projekts sein, aber auch ein Einzelmodul, was ich ggf. nur zum testen einzeln ausführen möchte. Die main()-Methode heißt bei mir auch immer genau so und ist sozusagen reserviert für den Ablauf bei einem Aufruf "von außen".

```
if __name__ == '__main__':
    main()
```

Wenn das "Hilfsmodul" (random Benennung zur Trennung) nur innerhalb von Python verwendet wird und nur von deinem "Eintrittsmodul" aufgerufen wird, brauchst du diese Abfrage nicht. Im "Hilfsmodul" gibt es dann natürlich auch keine `main()`-Mehtode.

In umfangreichen Projekten nenne ich das "Eintrittsmodule" dann meistens auch `main.py`. Es kommt aber auch vor, dass du zwei unabhängige "Eintrittsmodule" hast (bspw. GUI und Command-line Version) hast. Dann benenne ich die Dateien nach ihrer Funktion (bspw. `gui.py` und `command-line.py`). Aber ich glaube da gibt es keine einheitlichen Richtlinien und viele vernünftige Wege.



https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html#the-console-scripts-entry-point



the reason behind ``if __name__ == '__main__` is that you want to execute the line under the if only when the .py is called directly.

so it has a very specific use case.

if you are modules are only imported then you don’t have to worry about it.



### parameter vs. argument

parameter: the variable inside the function

argument: the value passed in the function call



### `os`

```
# Import `os`
import os

# Retrieve current working directory (`cwd`)
cwd = os.getcwd()
cwd

# Change directory
os.chdir("/path/to/your/folder")

# List all files and directories in current directory
os.listdir('.')
```



### Data types

integer: `41`		float: `41.99`		complex value: `41.999 + 0.1 * i`

tuple: `('data', 200)`

list (ordered sequence): `['Washington', 'Adams', 'Jefferson', ..., 'Obama', 'Trump']`

unordered sequence: `{'Obama', 'Adams', ..., 'Jefferson', 'Trump', 'Washington'}` (much faster than lists!)

dictionary: `{"John": 95, "Jennifer": 98}`



### Strings

##### f-strings (formatted)

use f before quotes and curly braces where to insert variable

```
>>> # Interpolate a dictionary
>>> grades = {"John": 95, "Jennifer": 98}
>>> f"Grades: {grades}"
"Grades: {'John': 95, 'Jennifer': 98}"
```

access attributes or call functions on variables:

```
>>> # Access an element in a list
>>> pets = ["Dogs", "Cats", "Turtles"]
>>> f"My pet: {pets[-1]}"
'My pet: Turtles'
>>> 
>>> # With a function call
>>> name = "john"
>>> f"Name: {name.title()}"
'Name: John'
>>> 
>>> # Some calculation
>>> number = 5
>>> f"Square: {number*number}"
'Square: 25'
```

Custom objects

```
>>> # Define a custom class
>>> class Student:
...     def __init__(self, name, age):
...         self.name = name
...         self.age = age
...
...     def __str__(self):
...         return f"Name: {self.name}; Age: {self.age}"
... 
>>> # Create an instance
>>> student = Student('John Smith', 17)
>>> 
>>> # Log the instance
>>> print(f"Student: {student}")
Student: Name: John Smith; Age: 17
```

When a custom instance is to be interpolated in the f-string, the `__str__()` is called to provide the interpolated value for the instance. 



##### r-strings

By prefixing a string literal with an r, we specify a raw string. In a raw string, the backslash character does not specify an escape sequence—it is a regular character.

```
raw = r"\directory\123"
val = "\directory\123"

print(raw)	# \directory\123
print(val)	# \directoryS
```

For file paths or folder paths, raw string literals are ideal. They  eliminate confusion about slashes or backslashes on Windows.



### pyperclip (store data in .txt)

Sending and receiving text to a clipboard.

```
import pyperclip

# store text in clipboard:
pyperclip.copy('Hello!')
# can now be pasted:
pyperclip.paste() # 'Hello!'
```



### logging / debugging

The logging module lets you display logging messages.
After calling `logging.basicConfig()` to set up logging, call `logging.debug(‘This is the message')` to create a log message.
When done, you can disable the log messages with `logging.disable(logging.CRITICAL)`
Don't use `print()` for log messages: It's hard to remove them all when you're done debugging.
The five log levels are: `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `CRITICAL`.

```python
import logging
logging.basicConfig(level=logging.DEBUG, format='%(message)s (%(levelname)s %(asctime)s)')

# log to file:
# logging.basicConfig(filename='myProgramLog.txt', level=logging.DEBUG, format='%(message)s (%(levelname)s %(asctime)s)')
# if uncommented: debug messages do not appear
# logging.disable(logging.CRITICAL)

# in program:
logging.debug()
```



### webbrowser

```
import webbrowser
webbrowser.open('http://inventwithpython.com/')
```

```
# Launch a map in the browser using an address from the command line or clipboard

import webbrowser, sys, pyperclip

if len(sys.argv) > 1:
    # Get address from command line.
    address = ' '.join(sys.argv[1:])
else:
    # Get address from clipboard.
    address = pyperclip.paste()
    
webbrowser.open('https://www.google.com/maps/place/' + address)
```



### build frontend

can be done easily with Streamlit



### requests / download files

```
import requests

res = requests.get('https://automatetheboringstuff.com/files/rj.txt')

res.status_code == requests.codes.ok  # True
# OR:
try:
    res.raise_for_status()  # raises an exception if there was an error downloading
except Exception as exc:
    print('There was a problem: %s' % (exc))

len(res.text)  # 178981
print(res.text[:250])  # (text is printed)

playFile = open('RomeoAndJuliet.txt', 'wb')  # open file in write binary mode (maintain 											 # Unicode encoding of the text)
for chunk in res.iter_content(100000):  # iter_content(): write the web page to a file in 										  # a for loop in chunks
    playFile.write(chunk)  # write() on each iteration to write the content to the file
playFile.close()  # close the file
```



### asynchronous

https://github.com/writeson/asynchronous_python_presentation



### Concurrency / parallel tasks

concurrent.futures

https://docs.python.org/3/library/concurrent.futures.html

Launching parallel tasks
CPU usage up to 100% - fast!



### try - except

```
try:
	raise Exception('spam', 'eggs')
except Exception as inst:
    print(type(inst))    # the exception instance
    print(inst.args)     # arguments stored in .args
    print(inst)          # __str__ allows args to be printed directly,
                         # but may be overridden in exception subclasses
```



### Numeric formatting

```
>>> # Big numbers separator
>>> big_number = 98765432123456789
>>> f"{big_number:_d}"
'98_765_432_123_456_789'
>>> 
>>> # Floating numbers formatting
>>> more_digits = 2.345678
>>> f"2 digits: {more_digits:.2f}; 4 digits: {more_digits:.4f}"
'2 digits: 2.35; 4 digits: 2.3457'
>>> 
>>> # Scientific notation
>>> sci_number = 0.0000043203
>>> f"number: {sci_number:e}"
'number: 4.320300e-06'
```



### String formatting

```
>>> s0, s1 = 'a', 'bb'
>>> 
>>> # Left-aligned with padding *
>>> print(f'{s0:*<7}\n{s1:*<7}')
a******
bb*****
>>> 
>>> # Right-aligned with padding %
>>> print(f'{s0:%>8}\n{s1:%>8}')
%%%%%%%a
%%%%%%bb
>>>
>>> # Center-aligned
>>> print(f'{s0:@^9}\n{s1:@^9}')
@@@@a@@@@
@@@bb@@@@
```



### Duplicates

`pd.unique()` might be an option

evtl. auch mit `set()`



### Importing/running modules

##### Using **runpy.run_module()** and **runpy.run_path()**

The [Python Standard Library](https://docs.python.org/3/library/index.html) has a module named runpy. run_module() is a function in runpy whose work is to execute modules without importing them in the first place. 

The module is located using import and then executed. The first argument of the run_module() must contain a string:

```
import runpy
runpy.run_module(mod_name='first_script')
Hello World!
{'__name__': 'first_script',
    ...
'_': None}}
```

Similarly, runpy contains another function run_path() which allows you to run a module by providing a location.

An example of such is as follows:

```
import runpy
runpy.run_path(file_path='first_script.py')
Hello World!
{'__name__': '',
    ...
'_': None}}
```

Both the functions return the globals dictionary of the executed module.

##### Using **exec()**

Other than the most commonly used ways to run Python scripts, there are other alternative ways. One such way is by using the built-in function exec(). It is used for the dynamic execution of Python code, be it a string or an object code.

An example of exec() is:

```
exec(open('first_script.py').read())
Hello World!
```



##### batch-file

The first line of all your Python programs should be a *shebang* line, which tells your computer that you want Python to execute this program. The shebang line begins with `#!`, but the rest depends on your operating system.

- On Windows, the shebang line is `#! python3`.
- On OS X, the shebang line is `#! /usr/bin/env python3`.
- On Linux, the shebang line is `#! /usr/bin/python3`.

To make it convenient to run your Python program, create a *.bat batch file* for running the Python program with *py.exe*. To make a batch file, make a new text file containing a single line like the following:

```
@py.exe C:\path\to\your\pythonScript.py %*
@pause		# necessary?
```

Replace this path with the absolute path to your own program, and save this file with a *.bat* file extension (for example, *pythonScript.bat*). This batch file will keep you from having to type the full absolute path for the Python program every time you want to run it. I recommend you place all your batch and *.py* files in a single folder, such as *C:\MyPythonScripts* or *C:\Users\YourName\PythonScripts*.

The *C:\MyPythonScripts* folder should be added to the system path on Windows so that you can run the batch files in it from the Run dialog. To do this, modify the `PATH` environment variable. Click the **Start** button and type **Edit environment variables for your account** ("Umgebungsvariablen"). This option should auto-complete after you’ve begun to type it.

From System variables, select the Path variable and click **Edit**. In the Value text field, append a semicolon, type **C:\MyPythonScripts**, and then click **OK**. Now you can run any Python script in the *C:\MyPythonScripts* folder by simply pressing `Win-R` and entering the script’s name. Running `pythonScript`, for instance, will run *pythonScript.bat*, which in turn will save you from having to run the whole command `py.exe C:\ MyPythonScripts\pythonScript.py` from the Run dialog.



### Pandas

```
# Inspect the shape (rows, columns)
data.shape

# Inspect the number of dimensions
data.ndim

# Inspect the data type
data.dtype

# Describe index (start, stop, increment)
data.index

# Describe DataFrame columns
data.columns

# Info on DataFrame
data.info()

# Number of non-NA values (lists row-names and the amount of values)
data.count()

# Summary statistics
data.describe()
```



### SQLite

give the DB a name, say ‘afos_steckbriefe’, and have the following command:

```
dbname = 'afos_steckbriefe'
conn = sqlite3.connect(dbname + '.sqlite')
```

At this point, SQLite is all set up and ready to be used in Python. Assuming we had some data loaded in the DB under *Table1*, we could execute SQL commands in the following way:

```
cur = conn.cursor()
cur.execute('SELECT * FROM Table1')

for row in cur:
    print(row)
```



##### Using Pandas to load data in our application

Assuming that we already have the data, we would like to analyse, we can use the Python Pandas library to do it.

First, we need to import the Pandas library and then we can load the data in a data frame (you can think of data frames as an array of sorts):

```
import pandas as pd

df = pd.read_excel('ourfile.xlsx'): df.columns = df.columns.str.replace(' ', '_')
```

(replace spaces with underscores for column names)

Once we have loaded the data, we can put it straight into our SQL Database with a simple command:

```
df.to_sql(name='Table1', con=conn)
```

If you are loading multiple files within the same table, you can use the *if_exists* parameter:

```
df.to_sql(name='Table1', con=conn, if_exists='append')
```

All together:

```
import sqlite3, pandas as pd, numpy as np

dbname = 'afos_steckbriefe'

conn = sqlite3.connect(dbname + '.sqlite')
cur = conn.cursor()

df = pd.read_excel('ourfile.xlsx'): df.columns = df.columns.str.replace(' ', '_')

df.to_sql(name='Table1', con=conn, if_exists='append')

cur.execute('SELECT * FROM afos_steckbriefe')

names = list(map(lambda x: x[0], cur.description)) #Returns the column names
print(names)
for row in cur:
    print(row)
cur.close()
```



```
t = ('RHAT',)
c.execute('SELECT * FROM stocks WHERE symbol=?', t)
print(c.fetchone())

# Larger example that inserts many records at a time
purchases = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
             ('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
             ('2006-04-06', 'SELL', 'IBM', 500, 53.00),
            ]
c.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases)
```



To retrieve data after executing a SELECT statement, you can either treat the cursor as an [iterator](https://docs.python.org/3.8/glossary.html#term-iterator), call the cursor’s [`fetchone()`](https://docs.python.org/3.8/library/sqlite3.html#sqlite3.Cursor.fetchone) method to retrieve a single matching row, or call [`fetchall()`](https://docs.python.org/3.8/library/sqlite3.html#sqlite3.Cursor.fetchall) to get a list of the matching rows.

```
for row in c.execute('SELECT * FROM stocks ORDER BY price'):
        print(row)
```

```
c.execute("select * from afos")
print(c.fetchall())
```

evtl.:

```
c.executemany("insert into afos values (?)", theIter)
```



Place 'r' before the path string to read any special characters, such as '\'

```
read_country = pd.read_csv (r'C:\Users\Ron\Desktop\Client\Country_14-JAN-2019.csv')
```



```
export_excel = df.to_excel (r'C:\Users\Ron\Desktop\Client\export_list.xlsx', index = None, header=True)
```



##### Database-queries

```
pd.read_sql("SELECT * FROM sales LIMIT 5", conn)
```



### Conditions

The usual way to do this is to state the function's goal. Then, you study the goal and consider how to meet it. Finally, you apply the laws for assignment and conditionals to show that the coding matches the specification. Often, you begin with just the function's postcondition and you formulate an appropriate precondition after writing the code.



##### precondition

The precondition statement indicates what must be true before the function is called.

The programmer who calls the function is responsible for ensuring that the precondition is valid/true when the function is called.

If you detect that a precondition has been violated, then print an error message and halt the program.

```
def recip(n) :   # HEADER LINE : LISTS NAME AND PARAMETERS
    """recip  computes the reciprocal for  n"""
    { pre   n != 0          # PRECONDITION : REQUIRED PROPERTY OF ARGUMENTS
      post  ans == 1.0 / n  # POSTCONDITION : THE GENERATED KNOWLEDGE
      return ans            # VARIABLE USED TO RETURN THE RESULT
    }
    ans = 1.0 / n   # BODY  (no assignment to parameter  n  allowed)
    return ans
```

The precondition states the situation under which the function operates correctly, and the postcondition states what the function has accomplished when it terminates.

```
def absValue(x) :
    { pre  x != 0
      post ans > 0
      return ans
    }
    if x < 0 :
        ans = 0 - x
    else :
        ans = x
    return ans
```



https://pypi.org/project/preconditions/

###### Basic type checking + multiple predicates

```
@preconditions(
	lambda i: isinstance(i, int) and i > 0,
)
def double(i):
    return 2*i
```

###### Multiple predicates & multiple arguments

```
@preconditions(
    lambda s: isinstance(s, unicode),
    lambda n: isinstance(n, int) and n >= 0,
    )
def repeat(s, n):
    return s*n
```

###### Default parameters within predicates

```
scores = {}

@preconditions(
    lambda color, _colors=['RED', 'GREEN', 'BLUE']: color in _colors
    )
def get_color_score(color):
    return scores[color]
```

###### Method preconditions

Predicates can be expressed for methods, including relations to `self`. For example, a `Monotonic` instance ensures that each call to `.set` must pass a value larger than any previous call:

```
class Monotonic (object):
    def __init__(self):
        self.v = 0

    @preconditions(lambda self, v: v > self.v)
    def set(self, v):
        self.v = v
```

Preconditions can be applied to special methods, such as `__new__`, `__init__`, `__call__`, etc…

```
class LinearRange (tuple):
    @preconditions(
           lambda a: isinstance(a, float),
           lambda b: isinstance(b, float),
           lambda a, b: a < b,
           )
    def __new__(cls, a, b):
        return super(OrderedTuple, cls).__new__(cls, (a, b))

    @preconditions(lambda w: 0 <= w < 1.0)
    def __call__(self, w):
        lo, hi = self
        return w * (hi - lo) + lo

    @preconditions(lambda x: self[0] <= x < self[1])
    def invert(self, x):
        lo, hi = self
        return (x - lo) / (hi - lo)
```



##### assertion

The assert function is useful for detecting violations of a precondition.

An assertion is a boolean expression at a specific point in a program which will be true unless there is a bug in the program. A test assertion is defined as an expression, which encapsulates some testable logic specified about a target under test.

The `assert` keyword lets you test if a condition in your code returns True, if not, the program will raise an AssertionError.

Programmers often place assertions at the start of a function to check for valid input, and after a function call to check for valid output.

```
def KelvinToFahrenheit(Temperature):
   assert (Temperature >= 0),"Colder than absolute zero!"
   return ((Temperature-273)*1.8)+32
```

```
numbers = [1.5, 2.3, 0.7, -0.001, 4.4]
total = 0.0
for n in numbers:
    assert n > 0.0, 'Data should only contain positive values'
    total += n
print 'total is:', total
```



##### postcondition

The postcondition statement indicates what will be true when the function finishes its work.

The programmer who writes the function counts on the precondition being valid, and ensures that the postcondition becomes true at the function’s end.

```
x = "hello"

#if condition returns False, AssertionError is raised:
assert x == "goodbye", "x should be 'hello'"
```



### Copy files from one folder to another

##### shutil

```
 shutil.copytree(src, dst, symlinks=False, ignore=None, copy_function=copy2, ignore_dangling_symlinks=False, dirs_exist_ok=False)¶

shutil.copytree('C:\\bacon', 'C:\\bacon_backup')  # might also create the folder "bacon_backup" --> check!

Recursively copy an entire directory tree rooted at src to a directory named dst and return the destination directory. dirs_exist_ok dictates whether to raise an exception in case dst or any missing parent directory already exists.
https://docs.python.org/3/library/shutil.html#shutil.copytree

Changed in version 3.8: Platform-specific fast-copy syscalls may be used internally in order to copy the file more efficiently. See Platform-dependent efficient copy operations section.
https://docs.python.org/3/library/shutil.html#shutil-platform-dependent-efficient-copy-operations
```



```
import shutil
import os
    
source_dir = '/path/to/source_folder'  # maybe / at the end
target_dir = '/path/to/dest_folder'	   # maybe / at the end
    
file_names = os.listdir(source_dir)
    
for file_name in file_names:
    shutil.move(os.path.join(source_dir, file_name), os.path.join(target_dir, file_name))	# but I don't want to move, I want to copy!
```



```
 shutil.copy2(src, dst, *, follow_symlinks=True)
 
When follow_symlinks is false, and src is a symbolic link, copy2() attempts to copy all metadata from the src symbolic link to the newly-created dst symbolic link. However, this functionality is not available on all platforms. On platforms where some or all of this functionality is unavailable, copy2() will preserve all the metadata it can; copy2() never raises an exception because it cannot preserve file metadata.
```



##### shutil & pathlib

```
from pathlib import Path
import shutil

src_path = '\tmp\files_to_move'
dest_path = '\vam\do'

for src_file in Path(src_path).glob('*.*'):
    shutil.copy(src_file, dest_path)
```

##### shutil & os

```
import os, shutil, glob

src_fldr = r"Source Folder/Directory path"
dst_fldr = "Destination Folder/Directory path"

try:
  os.makedirs(dst_fldr);		## create destination folder
except:
  print("Folder already exists or some error")

for txt_file in glob.glob(src_fldr+"\\*.txt"):	#  copy file with *.txt extension
    shutil.copy2(txt_file, dst_fldr)		# copy2 is better than copy
```

```
import glob, os, shutil

source_dir = '/path/to/dir/with/files' 			#Path where your files are at the moment
dst = '/path/to/dir/for/new/files'				#Path you want to move your files to
files = glob.iglob(os.path.join(source_dir, "*.txt"))
for file in files:
    if os.path.isfile(file):
        shutil.copy2(file, dst)		# copy2 is better than copy
```

##### os

```
names = os.listdir(src)		# List directory files

for name in names:
  srcname = os.path.join(src, name)
  dstname = os.path.join(dst, name)
  copy2(srcname, dstname)		# Copy files
```

```
def copy_myfile_dirOne_to_dirSec(src, dest, ext): 

    if not os.path.exists(dest):    # if dest dir is not there then we create here
        os.makedirs(dest);
        
    for item in os.listdir(src):
        if item.endswith(ext):
            s = os.path.join(src, item);
            fd = open(s, 'r');
            data = fd.read();
            fd.close();
            
            fname = str(item); #just taking file name to make this name file is destination dir     
            
            d = os.path.join(dest, fname);
            fd = open(d, 'w');
            fd.write(data);
            fd.close();
    
    print("Files are copied successfully")
```



### Search specific files in dir

```
import os# Get all the PDF filenames.pdfFiles = []    for filename in os.listdir('.'):        if filename.endswith('.pdf'):       	    pdfFiles.append(filename)pdfFiles.sort(key = str.lower) 		# sort list in alphabetical order
```



### Autorun Python script on startup in Windows

https://stackoverflow.com/questions/4438020/how-to-start-a-python-file-while-windows-starts

https://medium.com/better-programming/how-to-make-sense-of-distributed-processing-with-python-windows-services-9cfafc6fed2b

https://www.geeksforgeeks.org/autorun-a-python-script-on-windows-startup/

https://stackoverflow.com/questions/51622702/windows-10-run-python-program-in-startup



https://stackoverflow.com/questions/32404/how-do-you-run-a-python-script-as-a-service-in-windows

http://nssm.cc/



### Code formatting

Code formatting with tox, pre-commit, GitHub Actions
https://www.youtube.com/watch?v=OnM3KuE7MQM



### PDF

##### textract

https://github.com/deanmalmgren/textract

##### PDFminer.six

https://github.com/pdfminer/pdfminer.six

https://pdfminersix.readthedocs.io/en/latest/



##### PyPdf

```
import ioimport pyPdfimport PIL.Imageinfile_name = 'my.pdf'with open(infile_name, 'rb') as in_f:    in_pdf = pyPdf.PdfFileReader(in_f)    for page_no in range(in_pdf.getNumPages()):        page = in_pdf.getPage(page_no)        # Images are part of a page's `/Resources/XObject`        r = page['/Resources']        if '/XObject' not in r:            continue        for k, v in r['/XObject'].items():            vobj = v.getObject()            # We are only interested in images...            if vobj['/Subtype'] != '/Image' or '/Filter' not in vobj:                continue            if vobj['/Filter'] == '/FlateDecode':                # A raw bitmap                buf = vobj.getData()                # Notice that we need metadata from the object                # so we can make sense of the image data                size = tuple(map(int, (vobj['/Width'], vobj['/Height'])))                img = PIL.Image.frombytes('RGB', size, buf,                                          decoder_name='raw')                # Obviously we can't really yield here, do something with `img`...                yield img            elif vobj['/Filter'] == '/DCTDecode':                # A compressed image                img = PIL.Image.open(io.BytesIO(vobj._data))                yield img
```

I have some installation issues with pyPdf. I used PyPDF2 instead of pyPdf and replaced "yield" parts with `img.save(..)`



##### PyPdf2

https://github.com/mstamy2/PyPDF2/blob/master/Scripts/pdf-image-extractor.py

https://pdf.wondershare.com/how-to/extract-images-from-pdf-python.html



##### PyMuPDF

https://pymupdf.readthedocs.io/en/latest/

https://www.thepythoncode.com/article/extract-pdf-images-in-python

https://pdf.wondershare.com/how-to/extract-images-from-pdf-python.html

https://www.youtube.com/watch?v=QCasePBJxVI&t=0s



```
import fitz  # PyMuPDFdef get_pixmaps_in_pdf(pdf_filename):    doc = fitz.open(pdf_filename)    xrefs = set()    for page_index in range(doc.pageCount):        for image in doc.getPageImageList(page_index):            xrefs.add(image[0])  # Add XREFs to set so duplicates are ignored    pixmaps = [fitz.Pixmap(doc, xref) for xref in xrefs]    doc.close()    return pixmapsdef write_pixmaps_to_pngs(pixmaps):    for i, pixmap in enumerate(pixmaps):        pixmap.writePNG(f'{i}.png')  # Might want to come up with a better namepixmaps = get_pixmaps_in_pdf(r'C:\StackOverflow\09_chapter 4.pdf')write_pixmaps_to_pngs(pixmaps)
```

```
import fitzdoc = fitz.open(filePath)for i in range(len(doc)):    for img in doc.getPageImageList(i):        xref = img[0]        pix = fitz.Pixmap(doc, xref)        if pix.n < 5:       # this is GRAY or RGB            pix.writePNG("p%s-%s.png" % (i, xref))        else:               # CMYK: convert to RGB first            pix1 = fitz.Pixmap(fitz.csRGB, pix)            pix1.writePNG("p%s-%s.png" % (i, xref))            pix1 = None        pix = None
```



### XML

##### xml

https://docs.python.org/3/library/xml.html

http://www.academis.eu/posts/python_packages/xml/

##### BeautifulSoup (auch für xml)

https://pypi.org/project/beautifulsoup4/



### R

##### RPy2 (für R-Programme)

http://www.academis.eu/posts/python_packages/RPy/
